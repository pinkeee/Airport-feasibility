import csv 
import time

def air_distance(target, uk_airport, max_range, output, dist_between_airports): # takes in args
    with open("Airports.txt", "r") as csvfile: # "r" means the file will open in read mode
        csvreader = csv.reader(csvfile, delimiter=',')    # opens up file 'Airports.txt'
        fields = next(csvreader) 

        for line in csv.reader(csvfile): # for the lines in the file
            rows=(line) # adds all of the file to a var called rows
            for i in rows: 
                if i in target: # if arg target is in the file
                    ind=rows.index(i) # find index of it
                    #print(rows[ind])
                    for i in uk_airport: 
                        if "LPL" in uk_airport: # if uk_airport == LPL then
                            distance = ind + 2 # creates var called distance and takes the index of target and adds 2
                            distance=rows[distance] # finds what that equals in the file
                            dist_between_airports.append(int(distance)) # adds that to the arg 'dist_between_airports'
                        else: # if uk_airport == BOH then
                            distance = ind + 3 # creates var called distance and takes the index of target and adds 3
                            distance=rows[distance] # finds what that equals in the file
                            dist_between_airports.append(int(distance)) # adds that to the arg 'dist_between_airports'

                        # if the arg max_range is greater or equal to distance then
                        if (int(max_range[0]) >= int(distance)):
                            output.append(True) # add True to the arg output
                            return # returns
                        else:
                            # prints error message if above if statment is false
                            print("\nError, the selected aircraft's max distance is {}KM and the distance to {} from {} is {}KM.".format(max_range[0], target[0], uk_airport[0], distance))
                            # creates var go_back with input
                            go_back=str(input("Press enter to go back ..."))

                            if go_back=="": # if input is enter key then return to main menu
                                return



                  