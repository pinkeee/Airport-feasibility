# This file stores vars that i can call in any other file 
# this is to make it so i only have to type it once and it makes code look cleaner
MAINMENU=("""
    1. Enter airport details
    2. Enter flight details
    3. Enter price plan and calculte profit
    4. Clear data
    5. Quit\n
    >>> """)

UK_PORTS=("""
========================================
UK Airports:

LPL | Liverpool John Lennon airport 
    |
BOH | Bournemouth International airport
========================================
""")

OS_PORTS=("""
===============================================
Oversea's Airport
===============================================
""")

AIR_TYPES=("""
======================
Aircraft types
======================
""")

AIR_DETAILS=("""
============================================================================================
Aircraft            | Running cost per | Max flight | Capacity if all | Minimum number of  |
type                | seat per 100KM   | range (KM) | seats are       | first-class seats  |
                    |                  |            | standard-class  | (if there are any) |
____________________|__________________|____________|_________________|____________________|
""")

DATA_CALC=("""
No. standard class seats: {}
Flight cost per seat: £{}
Flight cost: £{}
Flight income: £{}
\nProfit of this flight: £{}
""")

"""
this file is just to store some vars, makes code look cleaner and better if it is split up plus its good practice too >_<
"""