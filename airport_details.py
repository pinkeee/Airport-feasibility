import csv
import time
from var import * 
from all_csv import *

def airport_details(args1, args2): # takes args 'args1' and 'args2'
    print(UK_PORTS) # prints UK_PORTS from var.py
    details=input("\nPlease enter the three-letter airport code for the UK airport: ").upper().strip() # creates var details and adds an input, .upper() means everything inputed will be turned into uppercase

    if details=="LPL".upper() or details=="BOH".upper(): # if input isnt either LPL or BOH then return to main menu
        args1.append(details) # if the input is either LPL or BOH add it to the array 'uk_airport'
        print(OS_PORTS) # prints OS_PORTS
        all_csv() # calls function all_csv from all_csv.py
        details=input("\nPlease enter the three-letter airport code for the Overseas airport: ").upper().strip() # ask user to input in over seas airport code

        with open("Airports.txt", "r") as csvfile: 
            csvreader = csv.reader(csvfile, delimiter=',')       # opens txt file 'Airports.txt'
            fields = next(csvreader)

            for line in csvreader: # for everything in the file
                rows=(line) # creates var with all of the text file data in
                for i in rows: # for everything in the file
                    if i == details: # if details is in the file then
                        ind=rows.index(i) # finds index of it
                        if (rows[ind]==details): # if the selected index data is equal to details then
                            args2.append(details) # adds details to args2
                            print(rows[ind+1]) # prints out the next index
                            print("Saved! Returning to main menu...")
                            time.sleep(2) # waits 2 seconds
                            return # returns to main menu
# error handling 
            else:
                print("Invalid airport code entered, returning to menu no changes will be saved.")
                time.sleep(2)                                                                                                              
    else:
        print("Invalid airport code entered, returning to menu no changes will be saved.")
        time.sleep(2)
        return
