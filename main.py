"""
====================================================================
    TO-DO:
        - LOOK AT MOVING FLIGHT_DETAILS INTO A SEPERATE FILE
====================================================================
"""

import csv
import os
import time
from var import * 
from airport_details import *
from air_distance import *

def main():
    # Creates array with data about the aircrafts 
    # Type, Running cost per seat per 100 km, Maximum flight range (km), Capacity if all seats are standard-class, Minimum number of first-class seats (if there are any)
    aircrafts=["Medium narrow body".upper(), 8, 2650, 180, 8,
                "Large narrow body".upper(), 7, 5600, 220, 10,
                "Medium wide body".upper(), 5, 4050, 406, 14]

    # For UK and Oversea's airports
    uk_airport=[]
    os_airport=[]

    # Type of aircraft
    air_type=[]

    # Number of first and standard class seats
    first_class=[]
    stand_seats=[]

    # Max range the aircraft can fly
    max_range=[]

    # Running cost per seat per 100km (for selected aircraft)
    run_cost=[]

    # Used in another file just to check if the plane can fly to the selected airport, this var only holds either True or nothing.
    max_distance_calc=[]

    # Capacity of standard seats
    cap_stand=[]

    # Distance between UK airport and oversea's airport (used in a function in air_distance.py)
    dist_uk_os=[]


    def clear(): os.system("clear");os.system("cls");return # makes func that will be used to clear interpreter (using 'clear' to clear linux terminal and 'cls' for windows

    def flight_details():
        print(AIR_TYPES) # prints var AIR_TYPES in var.py

        needed_types= aircrafts[0].format(), aircrafts[5], aircrafts[10] # adds all the aircraft name index's to array 
        print("{} | {} | {}".format(needed_types[0], needed_types[1], needed_types[2])) # prints it
        
        inp=input("\nEnter the type of aircraft: ").upper()
        
        for i in needed_types:
            if i == inp: # if input aircraft is in needed_type then
                air_type.append(inp) # add input aircraft to array air_type 
                
                print(AIR_DETAILS) # prints var called AIR_DETAILS from var.py
                ind=aircrafts.index(i) # grabs index of inputed aircraft 

                # finds index for all needed data about the aircraft
                running_cost=aircrafts[ind+1]
                max_r=aircrafts[ind+2]
                capacity_standard=aircrafts[ind+3]
                min_firstclass=aircrafts[ind+4]
                
                # adds all the data to arrays 
                run_cost.append(int(running_cost))
                max_range.append(int(max_r))
                cap_stand.append(capacity_standard)
                
                # prints data
                print("{}            {}             {}              {}                 {}".format(inp, running_cost, max_r, capacity_standard, min_firstclass))

                # input in a var called i
                i=input("\nEnter the number of first class seats on the aircraft: ")

                # program will try and see if it can chang the i var into an int called inp 
                try:
                    inp = int(i)
                # else print this error message 
                except ValueError:
                    print("\nError, input was not an int")
                    go_back=str(input("Press enter to return to main menu ..."))

                    if go_back=="": # if input is enter key then return to main menu
                        return

                # if inp does not equal 0 then
                if inp != 0:
                    # creates var called half, divs var capacity_standard by 2
                    half=capacity_standard/2
                    
                    # if inp is smaller than min_firstclass or inp is bigger than half then
                    if int(inp) < min_firstclass or int(inp) > half:
                        print("Error, please check your inputs and try again")
                        time.sleep(3)
                        return
                    else:
                        # add inp to array first_class
                        first_class.append(int(inp))
                        # creates var s and does capacity_stand - inp x 2 
                        s = capacity_standard - int(inp) * 2
                        # adds s to array stand_seats
                        stand_seats.append(s)
                        # prints data
                        print("Number of standard seats is: {}".format(str(s)))
                        print("Returning to main menu, all changes have been saved.")
                        # wait for 2 seconds then returns to main menu.
                        time.sleep(2)
                        return
        # if inp isn't in needed_type then display error message
        else:
            print("\nERROR, that input is invaild")

            # creates input and if enter is pressed returns to main menu
            go_back=str(input("Press enter to go back ..."))
            if go_back=="":
                return



    def calc_profit():    
        # calls function in another file air_distance.py
        air_distance(os_airport, uk_airport, max_range, max_distance_calc, dist_uk_os) # gives the function args
        
        # if all of theses arrays are empty then print error message
        if (uk_airport == [] or os_airport == []
            or air_type == [] or first_class == [] or max_distance_calc == []):
            print("Error, some values haven't been entered returning to main menu")
            time.sleep(2)
            return
        # else
        else:
            # 2 vars 2 inputs 
            i=input("Enter in the price of a standard seat: ")
            j=input("Enter in the price of a first class seat: ")

            # program trys and sees if the inputs can be put into int vars
            try:
                stand_price = int(i)
                first_price = int(j)
            # if it cant then displays error
            except ValueError:
                print("\nError, one of the inputs was not an int")
                go_back=str(input("Press enter to return to main menu ..."))

                if go_back=="": # if input is enter key then return to main menu
                    return

            # loops through the length of 'cap_stand'
            for i in range(len(cap_stand)):
                num_fs_seats = (first_class[i] + stand_seats[i]) # Number of first class seats plus number of standard class seats
                
                num_stand_seats = (cap_stand[i] - first_class[i] * 2) # Works out Number of standard-class seats (in figure 2)
                cost_per_seat = (int(run_cost[i]) * int(dist_uk_os[i]) / 100) # Flight cost per seat 
                flight_cost = (cost_per_seat * num_fs_seats) # The whole flight cost
                flight_income = (first_class[i] * first_price + stand_seats[i] * stand_price) # Ammount of income for the flight
                profit = (flight_income - flight_cost) # Flight profit

                # prints data
                print(DATA_CALC.format(num_stand_seats, cost_per_seat, flight_cost, flight_income, profit)) # prints out the var from var.py and adds in the needed data using string formatting.

                if profit < 0: # if profit is smaller than 0 then 
                    print("You would be losing money if you were to go ahead with this flight, it is not recommended!")
                    # no need for an else statment as the program will just continue on anyways.

                # once printed creates input, if enter is pressed then returns to main menu
                go_back=str(input("\nPress enter to go back ..."))

                if go_back=="":
                    return

    # this function is the main menu
    # when you return from a function you get sent back here
    # in the main menu.
    def menu(): 
        while True:
            try:
                clear()
                inp=input(MAINMENU).lower().strip() # .strip() just removes any whitespace in the input so the program doesnt fail if you put 1 with a space after it for example
            
                if inp=="1":
                    airport_details(uk_airport, os_airport)

                elif inp=="2":
                    flight_details()
                
                elif inp=="3":
                    calc_profit()

                elif inp=="4":
                    print("Cleared data!")
                    time.sleep(1)
                    main()

                elif inp=="5" or inp=="quit":
                    print("Qutting...")
                    time.sleep(1)
                    exit()

                elif inp != "1" or "2" or "3" or "4" or "5":
                    print("Invalid input, returning")
                    time.sleep(1)
                    menu()
                    
            except KeyboardInterrupt:
                print("Invalid input, returning")
                time.sleep(1)
                menu()
            
    menu()
main() 